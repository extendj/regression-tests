// A public top-level type must be declared in a file with the same name.
// https://bitbucket.org/extendj/extendj/issues/11/report-error-if-public-class-is-not
// .result=COMPILE_FAIL
public class Hodor {
}
