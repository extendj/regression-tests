// .result=COMPILE_FAIL

/*
    Inferred type initializer cannot be null
*/
public class Test {
    public static void main (String[] args) {
        var x = null;
    }

}