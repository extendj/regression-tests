// Brackets cannot be used in a var declaration.
// .result=COMPILE_FAIL

public class Test {
  public static void main (String[] args) {
    var x[] = 3;
  }
}
