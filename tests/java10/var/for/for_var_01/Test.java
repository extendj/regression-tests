// .result=EXEC_PASS

/*
 * Index variables declared in traditional for loops
 */
public class Test {
    public static void main(String[] args) {
        for(var i = 0; i < 2; i++){
            System.out.println(i);
        }
    }
}
