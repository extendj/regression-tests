// Inferred variable must have an initializer.
// .result=COMPILE_FAIL
public class Test {
  public static void main (String... args) {
    var x;
  }
}
